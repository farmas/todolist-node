var mongodb = require("mongodb");
var mongodbUri = require("mongodb-uri");
var mongoMiddleware = require("express-mongo-db");
var uri = process.env.MONGO_URI;
var mongoOptions = {
    hosts: [{
        host: "localhost",
        port: 27017
    }],
    database: "todolist"
};

if (uri) {
    mongoOptions = mongodbUri.parse(uri);
}

// Middleware that adds mongo db to the request object.
module.exports = mongoMiddleware(mongodb, mongoOptions);