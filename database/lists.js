var ObjectID = require('mongodb').ObjectID;

function getTodoListForUser(userId, req, res, next) {
    req.db.collection("lists").findOne({ _id: userId }, function(error, list) {
        if (error) {
            next(error);
        } else {
            req.todoList = list;
            next();
        }
    });
}

function updateTodoList(req, next, query, operation) {
    query = query || {};

    query["_id"] = req.user.id;

    req.db.collection("lists").update(
        query,
        operation,
        function(error) {
            if (error) {
                next(error);
            } else {
                next();
            }
        });
}

function addTask(req, task, next) {
    task.id = new ObjectID();
    updateTodoList(req, next, null, {
        "$push": { tasks: task }
    });
}

function deleteTask(req, taskId, next) {
    updateTodoList(req, next, null, {
        "$pull": { tasks: { "id": new ObjectID(taskId) }}
    });
}

function updateTask(req, task, next) {
    updateTodoList(req, next,
        {
            "tasks.id": new ObjectID(task.id)
        },
        {
            "$set": { "tasks.$.title": task.title, "tasks.$.completed": task.completed }
        });

}

function createList(userId, req, next) {
    // check if the list exists.
    getTodoListForUser(userId, req, null, function(error) {
        if (error) {
            next(error);
        } else if (req.todoList) {
            next();
        } else {
            // this is a new user, create a list for him/her.
            var todoList = {
                _id: userId,
                tasks: [
                {
                    id: new ObjectID(),
                    title: "My First Task",
                    completed: false
                },
                {
                    id: new ObjectID(),
                    title: "My Second Task",
                    completed: false
                }]
            };
            req.db.collection("lists").insertOne(todoList, function(error) {
                if (error) {
                    next(error);
                } else {
                    next();
                }
            });

            req.todoList = todoList;
            next();
        }
    });
}

module.exports = {
    createList: createList,
    addTask: addTask,
    deleteTask: deleteTask,
    updateTask: updateTask,
    middleware: {
        getTodoListForLoggedInUser: function(req, res, next) {
            return getTodoListForUser(req.user.id, req, res, next);
        }
    }
};