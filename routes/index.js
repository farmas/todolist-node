var express = require("express");
var auth = require("./auth");
var tasks = require("./tasks");
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index', {
    username: req.user && req.user.name,
    errors: req.flash("error")
  });
});

router.use("/auth", auth);
router.use("/tasks", tasks);

module.exports = router;