var express = require("express");
var dbLists = require('../database/lists');
var router = express.Router();

function addTask(req, res, next) {
     var task = {
        title: req.body.title,
        completed: false
    };

    dbLists.addTask(req, task, function(error) {
        if (error) {
            next(error);
        } else {
            res.set('Content-Type', 'application/json');
            res.send(JSON.stringify(task));
        }
    });
}

function updateTask(req, res, next) {
    var task = {
        id: req.body.id,
        title: req.body.title,
        completed: req.body.completed == "true"
    };

    dbLists.updateTask(req, task, function(error) {
        if (error) {
            next(error);
        } else {
            res.sendStatus(200);
        }
    })
}

function deleteTask(req, res, next) {
    var taskId = req.params.id;

    dbLists.deleteTask(req, taskId, function(error) {
        if (error) {
            next(error);
        } else {
            res.sendStatus(204);
        }
    })
}

// Routes
router.use(dbLists.middleware.getTodoListForLoggedInUser);

router.get("/", function(req, res) {
    res.set('Content-Type', 'application/json');
    res.send(JSON.stringify(req.todoList.tasks));
});

router.post("/", addTask);
router.put("/", updateTask);
router.delete("/:id", deleteTask);

module.exports = router;