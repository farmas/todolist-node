var express = require("express");
var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var db = require("../database/lists");

var router = express.Router();

var GOOGLE_CLIENT_ID = process.env.GOOGLE_CLIENT_ID;
var GOOGLE_CLIENT_SECRET = process.env.GOOGLE_CLIENT_SECRET;
var hostUrl = process.env.WEBSITE_URL || 'http://localhost:3000';

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
  done(null, obj);
});

passport.use(new GoogleStrategy({
    clientID: GOOGLE_CLIENT_ID,
    clientSecret: GOOGLE_CLIENT_SECRET,
    callbackURL: hostUrl + "/auth/google/callback",
    passReqToCallback: true
  },
  function(req, accessToken, refreshToken, profile, done) {
    db.createList(profile.id, req, function(error) {
      if (error) {
        done(error);
      } else {
        done(null, {
          id: profile.id,
          name: profile.displayName
        });
      }
    });
  }
));

router.get('/google', passport.authenticate('google', {
    scope: 'profile' }
));

router.get('/google/callback', passport.authenticate('google', {
    successRedirect: '/',
    failureRedirect: '/',
    failureFlash: "Login failed, please try again."
}));

router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

module.exports = router;
