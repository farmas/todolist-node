function Task(title, completed, id) {
    var self = this;
    this.id = ko.observable(id);
    this.title = ko.observable(title);
    this.completed = ko.observable(completed);

    this.completed.subscribe(function(val) {
        var data = ko.toJS(self);

        $.ajax("/tasks", {
            method: "PUT",
            data: data
        }).fail(function() {
            alert("Failed to update task, please reload the page.");
        });
    });
}

function ViewModel() {
    var self = this;
    this.tasks = ko.observableArray([]);

    this.addTask = function() {
        var $task = $("#newTask");
        var task = new Task($task.val());

        if (!task.title().trim()) {
            return;
        }

        self.tasks.push(task);
        $task.val("");

        $.ajax("/tasks", {
            method: "POST",
            data: {
                title: task.title()
            }
        }).fail(function() {
            alert("Failed to add task, please reload todo list.");
        }).done(function(t) {
            task.id(t.id);
        });
    }

    this.deleteTask = function(task) {
        var index = self.tasks.indexOf(task);
        self.tasks.splice(index, 1);

        $.ajax("/tasks/" + task.id(), {
            method: "DELETE",
        }).fail(function() {
            alert("Failed to delete task, please reload todo list.");
        });
    }
}

$(function() {
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);

    $.ajax("/tasks").done(function(data) {
        data.forEach(function(task) {
            viewModel.tasks.push(new Task(task.title, task.completed, task.id));
        });
    });
});